package com.example.xhan5.myapplication.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.xhan5.myapplication.MainActivity;
import com.example.xhan5.myapplication.MyService;
import com.example.xhan5.myapplication.PhoneReceiver;
import com.example.xhan5.myapplication.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *
 * to handle interaction events.
 * Use the {@link ServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceFragment extends BaseFragment {

    Button start;
    Button end;
    Button battery;
    Button incomingNumber;
    MainActivity mc;
    public ServiceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        start = view.findViewById(R.id.startButton);
        end = view.findViewById(R.id.endButton);
        battery = view.findViewById(R.id.batteryButton);
        incomingNumber = view.findViewById(R.id.incomingNumberButton);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mc,MyService.class);
                mc.startService(it);
            }
        });
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent stopit = new Intent(mc,MyService.class);
                mc.stopService(stopit);
            }
        });
        battery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = PreferenceManager
                        .getDefaultSharedPreferences(mc);
                int level = sp.getInt("batteryLevel", 0);
                Toast.makeText(mc,"BatteryLevel:"+level,Toast.LENGTH_LONG).show();
            }
        });
        incomingNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = PreferenceManager
                        .getDefaultSharedPreferences(mc);
                String level = sp.getString("incomingNumber", "0");
                Toast.makeText(mc,"incomingNumber:"+level,Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

    @Override
    public void onButtonPressed(Uri uri) {
        super.onButtonPressed(uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setActivity(MainActivity activity){
        this.mc = activity;
    }
}
