package com.example.xhan5.myapplication.task;

import android.os.AsyncTask;

import com.example.xhan5.myapplication.CommentActivity;
import com.example.xhan5.myapplication.TodoActivity;
import com.example.xhan5.myapplication.facade.WSfacade;

public class TodoTask extends AsyncTask<String,Integer,String> {
    TodoActivity act;
    public TodoTask(TodoActivity act){
        this.act = act;
    }
    @Override
    protected String doInBackground(String... strings) {
        return WSfacade.singleton().getTodo(act,strings[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        act.ReceiveData(s);
    }
}
