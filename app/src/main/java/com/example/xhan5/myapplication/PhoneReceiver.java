package com.example.xhan5.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;

public class PhoneReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String resultData = this.getResultData();

        if (action.equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            // 去电，可以用定时挂断
            // 双卡的手机可能不走这个Action
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

        } else {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            //manager.listen(new PhoneStateListener(), PhoneStateListener.LISTEN_CALL_STATE);

            MyPhoneStateListener listener=new MyPhoneStateListener();

            listener.context = context;
            manager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
        }


    }



class MyPhoneStateListener extends PhoneStateListener{
    public Context context;
    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
//注意，方法必须写在super方法后面，否则incomingNumber无法获取到值。
        super.onCallStateChanged(state, incomingNumber);

        System.out.println(incomingNumber);

        switch(state){
            case TelephonyManager.CALL_STATE_IDLE:
                System.out.println("挂断");
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                System.out.println("接听");
                break;
            case TelephonyManager.CALL_STATE_RINGING:
                System.out.println("响铃:来电号码"+incomingNumber);

                            SharedPreferences sp = PreferenceManager
                                    .getDefaultSharedPreferences(context);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("incomingNumber",incomingNumber);
                            editor.commit();

                break;
        }
    }
}

}