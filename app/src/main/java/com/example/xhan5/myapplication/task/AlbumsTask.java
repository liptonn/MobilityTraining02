package com.example.xhan5.myapplication.task;

import android.os.AsyncTask;

import com.example.xhan5.myapplication.facade.WSfacade;
import com.example.xhan5.myapplication.view.AlbumsFragment;

public class AlbumsTask extends AsyncTask<String,Integer,String> {
    AlbumsFragment frag;
    public AlbumsTask(AlbumsFragment frag){
        this.frag = frag;
    }

    @Override
    protected String doInBackground(String... strings) {
        return WSfacade.singleton().getAlbums(frag.getContext());
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        frag.ReceiveData(s);
    }
}
