package com.example.xhan5.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.xhan5.myapplication.task.CommentsTask;
import com.example.xhan5.myapplication.task.TodoTask;
import com.example.xhan5.myapplication.view.BaseFragment;
import com.example.xhan5.myapplication.view.MyCommentsRecyclerViewAdapter;
import com.example.xhan5.myapplication.view.MyTodoRecyclerViewAdapter;
import com.example.xhan5.myapplication.vo.Comments;
import com.example.xhan5.myapplication.vo.Todos;
import com.example.xhan5.myapplication.vo.Users;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class TodoActivity extends AppCompatActivity implements BaseFragment.OnListFragmentInteractionListener {
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    MyTodoRecyclerViewAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        Intent it = getIntent();
        String id =  it.getStringExtra("user_id");
        mRecyclerView = (RecyclerView) findViewById(R.id.todo_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setReverseLayout(false);//true 从底部开始填充子视图
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));

        TodoTask task = new TodoTask(this);
        task.execute(id);

    }

    public void ReceiveData(String response){
        List<Todos> list = new Gson().fromJson(response,new TypeToken<List<Todos>>(){}.getType());
        if(list.size()>0){
            mRecyclerView.setAdapter(new MyTodoRecyclerViewAdapter(list, this));
        }
    }

    @Override
    public void onListFragmentInteraction(Object uri) {
        Toast.makeText(this,"todo 点击",Toast.LENGTH_LONG).show();
        Todos todo = (Todos)uri;
        Intent intent=new Intent();
        if(todo.isCompleted())
        intent.putExtra("complete","已完成");
        else
        intent.putExtra("complete","未完成");

        setResult(0,intent);
        finish();
    }
}
