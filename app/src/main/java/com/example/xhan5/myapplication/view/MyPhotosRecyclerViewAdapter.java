package com.example.xhan5.myapplication.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xhan5.myapplication.R;
import com.example.xhan5.myapplication.view.BaseFragment.OnListFragmentInteractionListener;
import com.example.xhan5.myapplication.dummy.DummyContent.DummyItem;
import com.example.xhan5.myapplication.vo.Photos;

import java.net.URL;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPhotosRecyclerViewAdapter extends RecyclerView.Adapter<MyPhotosRecyclerViewAdapter.ViewHolder> {

    private final List<Photos> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyPhotosRecyclerViewAdapter(List<Photos> items, BaseFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_photos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mImgView.setImageDrawable(null);
        holder.mImgView.setImageDrawable(null);
        final String httpUrl = holder.mItem.getThumbnailUrl();
        holder.mImgView.setTag(httpUrl);

        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    URL picUrl = new URL(httpUrl);
                    Bitmap bitmap = BitmapFactory.decodeStream(picUrl.openStream());
                    return bitmap;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //加载完毕后判断该imageView等待的图片url是不是加载完毕的这张
                //如果是则为imageView设置图片,否则说明imageView已经被重用到其他item
                if (httpUrl.equals(holder.mImgView.getTag())) {
                    holder.mImgView.setImageBitmap((Bitmap) o);
                }
            }
        }.execute();

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                   // mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImgView;
        public Photos mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImgView = (ImageView) view.findViewById(R.id.picture);
        }

        @Override
        public String toString() {
            return super.toString() + " '"  + "'";
        }
    }
}
