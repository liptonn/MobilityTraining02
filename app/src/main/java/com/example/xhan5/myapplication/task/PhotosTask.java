package com.example.xhan5.myapplication.task;

import android.os.AsyncTask;

import com.example.xhan5.myapplication.facade.WSfacade;
import com.example.xhan5.myapplication.view.PhotosFragment;
import com.example.xhan5.myapplication.view.postsFragment;

public class PhotosTask extends AsyncTask<String,Integer,String> {
    PhotosFragment frag;
    public PhotosTask(PhotosFragment frag){
        this.frag = frag;
    }
    @Override
    protected String doInBackground(String... strings) {
        return WSfacade.singleton().getPhotos(frag.getContext());
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        frag.ReceiveData(s);
    }
}
