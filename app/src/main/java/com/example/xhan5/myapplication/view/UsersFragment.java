package com.example.xhan5.myapplication.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.xhan5.myapplication.CommentActivity;
import com.example.xhan5.myapplication.R;
import com.example.xhan5.myapplication.TodoActivity;
import com.example.xhan5.myapplication.task.PostsTask;
import com.example.xhan5.myapplication.task.UsersTask;
import com.example.xhan5.myapplication.view.dummy.DummyContent;
import com.example.xhan5.myapplication.view.dummy.DummyContent.DummyItem;
import com.example.xhan5.myapplication.view.dummy.UsersContent;
import com.example.xhan5.myapplication.vo.Photos;
import com.example.xhan5.myapplication.vo.Posts;
import com.example.xhan5.myapplication.vo.Users;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class UsersFragment extends BaseFragment implements BaseFragment.OnListFragmentInteractionListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    RecyclerView recyclerView;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UsersFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static UsersFragment newInstance(int columnCount) {
        UsersFragment fragment = new UsersFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        UsersTask task = new UsersTask(this);
        task.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_users_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
           // recyclerView.setAdapter(new MyUsersRecyclerViewAdapter(UsersContent.ITEMS, this));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public void ReceiveData(String response){
        List<Users> list = new Gson().fromJson(response,new TypeToken<List<Users>>(){}.getType());
        if(list.size()>0){
            recyclerView.setAdapter(new MyUsersRecyclerViewAdapter(list, this));
        }
    }
    @Override
    public void onListFragmentInteraction(Object uri) {
        Users user = (Users)uri;
        Intent intent = new Intent(this.getContext(), TodoActivity.class);
        intent.putExtra("user_id", user.getId().toString());
        Toast.makeText(this.getContext(),"user 点击",Toast.LENGTH_LONG).show();
        startActivityForResult(intent,0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this.getContext(),"user获取返回值",Toast.LENGTH_LONG).show();
        String complete=data.getStringExtra("complete");
        Toast.makeText(this.getContext(),complete,Toast.LENGTH_LONG).show();
    }
}
