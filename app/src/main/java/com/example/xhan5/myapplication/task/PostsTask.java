package com.example.xhan5.myapplication.task;

import android.os.AsyncTask;

import com.example.xhan5.myapplication.MainActivity;
import com.example.xhan5.myapplication.facade.WSfacade;
import com.example.xhan5.myapplication.view.postsFragment;

public class PostsTask extends AsyncTask<String,Integer,String> {
   postsFragment frag;
    public PostsTask(postsFragment frag){
        this.frag = frag;
    }
    @Override
    protected String doInBackground(String... strings) {
        return WSfacade.singleton().getPosts(frag.getContext());
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        frag.ReceiveData(s);
    }
}
