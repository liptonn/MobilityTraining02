package com.example.xhan5.myapplication.task;

import android.os.AsyncTask;

import com.example.xhan5.myapplication.facade.WSfacade;
import com.example.xhan5.myapplication.view.UsersFragment;
import com.example.xhan5.myapplication.view.postsFragment;

public class UsersTask extends AsyncTask<String,Integer,String> {
   UsersFragment frag;
    public UsersTask(UsersFragment frag){
        this.frag = frag;
    }
    @Override
    protected String doInBackground(String... strings) {
        return WSfacade.singleton().getUsers(frag.getContext());
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        frag.ReceiveData(s);
    }
}
