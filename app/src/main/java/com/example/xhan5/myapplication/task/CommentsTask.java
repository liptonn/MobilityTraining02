package com.example.xhan5.myapplication.task;

import android.os.AsyncTask;

import com.example.xhan5.myapplication.CommentActivity;
import com.example.xhan5.myapplication.facade.WSfacade;
import com.example.xhan5.myapplication.view.postsFragment;

public class CommentsTask extends AsyncTask<String,Integer,String> {
   CommentActivity act;
    public CommentsTask(CommentActivity act){
        this.act = act;
    }
    @Override
    protected String doInBackground(String... strings) {
        return WSfacade.singleton().getComments(act,strings[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        act.ReceiveData(s);
    }
}
