package com.example.xhan5.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class BatteryReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();//获取意图中所有的附加信息
        //获取当前电量，总电量
        int level = extras.getInt(BatteryManager.EXTRA_LEVEL/*当前电量*/, 0);
        int total = extras.getInt(BatteryManager.EXTRA_SCALE/*总电量*/, 100);
        System.out.println("电池电量:"+level+"|总电量:"+total);
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("batteryLevel", level);
        editor.commit();
    }
}
