package com.example.xhan5.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.xhan5.myapplication.task.CommentsTask;
import com.example.xhan5.myapplication.view.MyCommentsRecyclerViewAdapter;
import com.example.xhan5.myapplication.view.MypostsRecyclerViewAdapter;
import com.example.xhan5.myapplication.vo.Comments;
import com.example.xhan5.myapplication.vo.Posts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class CommentActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    MyCommentsRecyclerViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Intent it = getIntent();
        String id =  it.getStringExtra("post_id");
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setReverseLayout(false);//true 从底部开始填充子视图
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));

        CommentsTask task = new CommentsTask(this);
        task.execute(id);

    }

    public void ReceiveData(String response){
        List<Comments> list = new Gson().fromJson(response,new TypeToken<List<Comments>>(){}.getType());
        if(list.size()>0){
            mRecyclerView.setAdapter(new MyCommentsRecyclerViewAdapter(list));
        }
    }
}
