package com.example.xhan5.myapplication.facade;

import org.json.JSONObject;

import java.io.IOException;

public interface OkHttpCallback {
    /**
     * 响应成功
     */
    //void onSuccess(JSONObject oriData, ServerResponse response);


    /**
     * 响应失败
     */
    void onFailure(IOException e);

}
