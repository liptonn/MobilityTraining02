package com.example.xhan5.myapplication;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xhan5.myapplication.view.AlbumsFragment;
import com.example.xhan5.myapplication.view.BaseFragment;
import com.example.xhan5.myapplication.view.PhotosFragment;
import com.example.xhan5.myapplication.view.ServiceFragment;
import com.example.xhan5.myapplication.view.UsersFragment;
import com.example.xhan5.myapplication.view.dummy.AlbumsContent;
import com.example.xhan5.myapplication.view.dummy.DummyContent;
import com.example.xhan5.myapplication.view.dummy.UsersContent;
import com.example.xhan5.myapplication.view.postsFragment;
import com.example.xhan5.myapplication.vo.Photos;

public class MainActivity extends AppCompatActivity implements BaseFragment.OnListFragmentInteractionListener {

    private TextView mTextMessage;
    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
    AlbumsFragment albumsFragment;
    UsersFragment usersFragment;
    postsFragment pFragment;
    PhotosFragment phFragment;
    ServiceFragment serviceFragment;
    private Fragment currentFragment=new Fragment();
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    showFragment(pFragment);
                    return true;
                case R.id.navigation_dashboard:
                    showFragment(usersFragment);
                    return true;
                case R.id.navigation_notifications:
                    showFragment(albumsFragment);
                    return true;
                case R.id.navigation_photos:
                    showFragment(phFragment);
                    return true;
                case R.id.navigation_service:
                    showFragment(serviceFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BatteryReceiver bb = new BatteryReceiver();
        // 创建意图对象
        IntentFilter iFilter = new IntentFilter();
        // 添加电池改变的活动
        iFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(bb, iFilter);

        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("batteryLevel", 100);
        editor.commit();

        initFragment();
        showFragment(pFragment);
        //mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }
    private void initFragment(){
        albumsFragment = new AlbumsFragment();
        usersFragment = new UsersFragment();
        pFragment = new postsFragment();
        phFragment = new PhotosFragment();
        serviceFragment = new ServiceFragment();
        serviceFragment.setActivity(this);
    }
    private void showFragment(Fragment fragment) {
        if (currentFragment!=fragment) {
            FragmentTransaction transaction = manager.beginTransaction();
           transaction.hide(currentFragment);
               currentFragment = fragment;
              if (!fragment.isAdded()) {
                transaction.add(R.id.fl_content, fragment).show(fragment).commit();
             } else {
                transaction.show(fragment).commit();
                }
            }
    }
    @Override
    public void onListFragmentInteraction(Object obj) {
        if(obj instanceof AlbumsContent.Album){
            AlbumsContent.Album item = ( AlbumsContent.Album)obj;
            Toast.makeText(this,"Album : "+item.content,Toast.LENGTH_LONG).show();
        }else if(obj instanceof UsersContent.User){
            UsersContent.User item = ( UsersContent.User)obj;
            Toast.makeText(this,"User : " + item.content,Toast.LENGTH_LONG).show();
        }else{
            DummyContent.DummyItem item = ( DummyContent.DummyItem)obj;
            Toast.makeText(this,"Dummy : "+ item.content,Toast.LENGTH_LONG).show();
        }

    }
}
