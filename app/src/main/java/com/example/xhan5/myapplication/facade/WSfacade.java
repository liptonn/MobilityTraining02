package com.example.xhan5.myapplication.facade;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WSfacade {

    private static WSfacade _inst=null;
    public static WSfacade singleton(){
        if(_inst == null){
            _inst=new WSfacade();
        }
        return _inst;
    }

    public String getPosts(Context context) {
        String action = "posts";
        HashMap<String, Object> param = new HashMap<String, Object>();
        return OkHttpManager.getInstance(context).getBySyn(action,param);

    }
    public String getUsers(Context context) {
        String action = "users";
        HashMap<String, Object> param = new HashMap<String, Object>();
        return OkHttpManager.getInstance(context).getBySyn(action,param);

    }
    public String getAlbums(Context context) {
        String action = "albums";
        HashMap<String, Object> param = new HashMap<String, Object>();
        return OkHttpManager.getInstance(context).getBySyn(action,param);

    }

    public String getComments(Context context,String id) {
        String action = "comments";
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("postId",id);
        return OkHttpManager.getInstance(context).getBySyn(action,param);

    }
    public String getTodo(Context context,String id) {
        String action = "todos";
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("userId",id);
        return OkHttpManager.getInstance(context).getBySyn(action,param);

    }
    public String getPhotos(Context context) {
        String action = "photos";
        HashMap<String, Object> param = new HashMap<String, Object>();
        return OkHttpManager.getInstance(context).getBySyn(action,param);

    }
}
